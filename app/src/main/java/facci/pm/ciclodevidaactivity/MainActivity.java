package facci.pm.ciclodevidaactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("Ciclo de vida", "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Ciclo de vida", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Ciclo de vida", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("Ciclo de vida", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("Ciclo de vida", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("Ciclo de vida", "onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("Ciclo de vida", "onRestart");
    }
}
